LOCAL_PATH := $(call my-dir)

# search for the openal built on cmake side

include $(CLEAR_VARS)

ifeq ($(NDK_DEBUG),1)
	SOL_LIB_SUFFIX=d
	SOL_LIB_CONFIG=debug
else
	SOL_LIB_SUFFIX=
	SOL_LIB_CONFIG=release
endif

LOCAL_MODULE := openal
LOCAL_SRC_FILES := ../../../cmake_build/build/intermediates/cmake/$(SOL_LIB_CONFIG)/obj/$(TARGET_ARCH_ABI)/libopenal.so
$(warning $(LOCAL_SRC_FILES))
LOCAL_EXPORT_C_INCLUDES := ../cmake_build/jni/openal-soft/include/AL/
ifneq ($(wildcard $(LOCAL_PATH)/$(LOCAL_SRC_FILES)),)
  include $(PREBUILT_SHARED_LIBRARY)
else
  LOCAL_SRC_FILES :=
  include $(BUILD_SHARED_LIBRARY)
endif
