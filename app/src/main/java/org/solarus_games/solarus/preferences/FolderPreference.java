package org.solarus_games.solarus.preferences;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.preference.DialogPreference;
import android.util.AttributeSet;

import org.solarus_games.solarus.R;

public class FolderPreference extends DialogPreference {
    public String mPath;

    public FolderPreference(Context context) {
        this(context, null);
    }

    public FolderPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FolderPreference(Context context, AttributeSet attrs,
                          int defStyleAttr) {
        this(context, attrs, defStyleAttr, defStyleAttr);
    }

    public FolderPreference(Context context, AttributeSet attrs,
                          int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        //TODO ?
        setPositiveButtonText("Ok");
        setNegativeButtonText("Cancel");
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;

        //Saved to shared preferences
        persistString(mPath);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        // Default value from attribute. Fallback value is set to 0.
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue,
                                     Object defaultValue) {
        // Read the value. Use the default value if it is not possible.
        setPath(restorePersistedValue ?
                getPersistedString(mPath) : (String) defaultValue);
    }

    @Override
    public int getDialogLayoutResource() {
        return R.layout.preference_folder;
    }
}
