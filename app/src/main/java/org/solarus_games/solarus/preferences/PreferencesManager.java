package org.solarus_games.solarus.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import static android.support.v7.preference.PreferenceManager.getDefaultSharedPreferences;

public class PreferencesManager {
    private final static long VIBRATION_DURATION = 20; // ms

    static SharedPreferences prefs;

    public static void init(Context context) {
        prefs = getDefaultSharedPreferences(context);
    }

    public static boolean isGamepadEnabled() {
        return prefs.getBoolean("virtual_gamepad", true);
    }

    public static long getVibrationDuration() {
        return VIBRATION_DURATION;
    }

    public static boolean isVibrationEnabled() {
        return prefs.getBoolean("virtual_gamepad_vibrate", true);
    }

    public static boolean isVibrateDpad() {
        return prefs.getBoolean("virtual_gamepad_dpad_vibrate", true);
    }

    public static boolean isAudioEnabled() {
        return prefs.getBoolean("audio", true);
    }


    public static boolean isIgnoreLayoutSizePreferencesEnabled() {
        return prefs.getBoolean("virtual_gamepad_ignore_size", false);
    }

    public static int getLayoutTransparency() {
        return prefs.getInt("virtual_gamepad_opacity", 128);
    }

    public static int getLayoutSize() {
        return prefs.getInt("virtual_gamepad_size", 100);
    }


    public static boolean isForcedLandscape() {
        return prefs.getBoolean("force_landscape", false);
    }
}
