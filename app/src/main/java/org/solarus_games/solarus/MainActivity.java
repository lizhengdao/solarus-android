package org.solarus_games.solarus;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import org.solarus_games.solarus.about.AboutFragment;
import org.solarus_games.solarus.preferences.PreferencesActivity;
import org.solarus_games.solarus.preferences.PreferencesFragment;
import org.solarus_games.solarus.quest.PausedQuestFragment;
import org.solarus_games.solarus.quest.Quest;
import org.solarus_games.solarus.quest.QuestInfoActivity;
import org.solarus_games.solarus.quest.QuestListFragment;


public class MainActivity extends AppCompatActivity
        implements QuestListFragment.OnListFragmentInteractionListener,
        PausedQuestFragment.OnFragmentInteractionListener,
        NavigationView.OnNavigationItemSelectedListener,
        AboutFragment.OnFragmentInteractionListener {

    private final int REQUEST_READ_STORAGE = 124;
    private final String TAG = "EngineActivity";
    private final static String QUEST_DETAILS_DIALOG_TAG = "quest_dialog";
    private QuestListFragment mQuestList;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private PausedQuestFragment mPausedQuestFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean hasPermission = (ContextCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) &&
                (ContextCompat.checkSelfPermission(getBaseContext(),
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);

        onNavigationItemSelected(R.id.nav_quests);
        setTitle("Quests"); //TODO find cleaner way

        if (!hasPermission) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_READ_STORAGE);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextAppearance(this, R.style.ToolBarTitle);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mNavigationView = navigationView;

        checkIntentForQuest(getIntent());
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        setIntent(intent);
        checkIntentForQuest(intent);
    }

    private void checkIntentForQuest(Intent intent) {
        //Does the activity was launched with a quest to start ?
        String qp = getIntent().getStringExtra(EngineActivity.ARG_PATH);
        if(qp != null) {
            Quest q = Quest.fromPath(qp);
            questLaunchRequest(getApplicationContext(), q);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        // set item as selected to persist highlight
        mNavigationView.setCheckedItem(menuItem.getItemId());
        // close drawer when item is tapped
        mDrawerLayout.closeDrawers();

        // Add code here to update the UI based on the item selected
        // For example, swap UI fragments here
        return onNavigationItemSelected(menuItem.getItemId());
    }

    public boolean onNavigationItemSelected(int itemIndex) {
        Fragment fragment = mQuestList;
        Intent i = null;
        switch(itemIndex) {
            case R.id.nav_settings:
                i = new Intent(this, PreferencesActivity.class);
                break;
            case R.id.nav_about: //TODO set the AboutActivity intent here
                fragment = new AboutFragment();
                break;
        }

        /*getSupportFragmentManager().beginTransaction()
                .replace(R.id.frame_layout, fragment)
                .commit();*/

        if(i != null) {
            startActivity(i);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_READ_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    finish();
                    startActivity(getIntent());
                    //reload my activity with permission granted or use the features what required the permission
                } else
                {
                    Toast.makeText(getBaseContext(), "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    ///List related methods
    @Override
    public void onQuestDetailClick(Quest quest) {
        Intent intent = new Intent(getApplicationContext(), QuestInfoActivity.class);
        intent.putExtra(QuestInfoActivity.ARG_PATH, quest.path);
        startActivity(intent);
    }

    public static Intent questLaunchIntent(Context context, Quest quest) {
        Intent intent = new Intent(context, EngineActivity.class);
        intent.putExtra(EngineActivity.ARG_PATH, quest.path);
        return intent;
    }

    public static void launchQuest(Context context, Quest quest) {
        Intent intent = questLaunchIntent(context, quest);
        if(quest.equals(SolarusApp.getCurrentQuest())) {
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }
        context.startActivity(intent);
    }

    public static Intent shortcutIntent(Context context, Quest quest) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EngineActivity.ARG_PATH, quest.path);
        return intent;
    }

    /**
     * Launch a quest but take care of closing any already existing
     *
     * @param quest
     */
    public static void questLaunchRequest(final Context context, final Quest quest) {
        if(SolarusApp.mustExitBeforeLaunch(quest)) {

            AlertDialog alertDialog = new AlertDialog.Builder(context).create();
            alertDialog.setTitle("Another quest is running");
            alertDialog.setMessage("Quit current quest?");
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SolarusApp.finishCurrentEngine();
                            launchQuest(context, quest);
                            dialog.dismiss();
                        }
                    });
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //Do nothing...
                        }
                    });
            alertDialog.show();
        } else {
            launchQuest(context, quest);
        }
    }

    @Override
    public void onQuestQuitRequest(Quest q) {
        SolarusApp.finishCurrentEngine();
        mQuestList.notifyDataSetChanged(); //Notify that a quest has lost the "Now playing" status
        mPausedQuestFragment.update();
    }

    @Override
    public void onQuestResumeRequest(Quest quest) {
        questResumeRequest(getApplicationContext(), quest);
    }
    /**
     * request to resume an already running quest
     *
     * @param context
     * @param quest
     */
    public static void questResumeRequest(Context context, Quest quest) {
        Intent intent = new Intent(context, EngineActivity.class);
        if(quest.equals(SolarusApp.getCurrentQuest())) {
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }
        intent.putExtra(EngineActivity.ARG_PATH, quest.path);
        context.startActivity(intent);
    }

    @Override
    public void attachPausedQuestFragment(PausedQuestFragment f) {
        mPausedQuestFragment = f;
    }

    @Override
    public void attachQuestListFragment(QuestListFragment f) {
        mQuestList = f;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
