package org.solarus_games.solarus.quest;



import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.pm.ShortcutInfoCompat;
import android.support.v4.content.pm.ShortcutManagerCompat;
import android.support.v4.graphics.drawable.IconCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.solarus_games.solarus.MainActivity;
import org.solarus_games.solarus.R;
import org.solarus_games.solarus.SolarusApp;


/**
 * An activity that present information about quest and
 * various options to launch /resume / shortcut it
 */
public class QuestInfoActivity extends AppCompatActivity {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_PATH = "path";

    private Quest mQuest;
    private Button mPlayButton;
    private Button mResumeButton;
    private Button mQuitButton;
    private TextView mNowPlaying;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mQuest = Quest.fromPath(getIntent().getExtras().getString(ARG_PATH));

        setContentView(R.layout.activity_quest_info);

        // Quest title
        TextView title = findViewById(R.id.quest_title);
        title.setText(mQuest.title);

        // Quest author
        TextView author = findViewById(R.id.quest_author);
        author.setText(mQuest.author);

        TextView release_date = findViewById(R.id.release_date);
        release_date.setText(mQuest.release_date);

        // Quest logo
        ImageView logo = findViewById(R.id.quest_logo);
        logo.setImageBitmap(mQuest.logo);

        // Quest icon
        ImageView icon = findViewById(R.id.quest_icon);
        icon.setImageBitmap(mQuest.icon);

        // Quest description
        TextView description = findViewById(R.id.description_text);
        description.setText(mQuest.longDescription);

        // Quest version
        TextView version = findViewById(R.id.version_text);
        version.setText(mQuest.version);

        // Quest format
        TextView format = findViewById(R.id.format_text);
        format.setText(mQuest.format);

        // Play button
        mPlayButton = findViewById(R.id.play_button);
        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlayButtonPressed();
            }
        });

        // Quit button
        mQuitButton = findViewById(R.id.quit_button);
        mQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onQuitButtonPressed();
            }
        });

        // Resume button
        mResumeButton = findViewById(R.id.resume_button);
        mResumeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResumeButtonPressed();
            }
        });

        Button shortcutButton = findViewById(R.id.shortcut_button);
        shortcutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShortcutButtonPressed();
            }
        });

        mNowPlaying = findViewById(R.id.now_playing);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextAppearance(this, R.style.ToolBarTitle);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public void questShortcutRequest(Quest q)
    {
        Context context = this;
        if (ShortcutManagerCompat.isRequestPinShortcutSupported(context))
        {
            ShortcutInfoCompat shortcutInfo = new ShortcutInfoCompat.Builder(context, q.title)
                    .setIntent(MainActivity.shortcutIntent(this, q).setAction(Intent.ACTION_MAIN)) // !!! intent's action must be set on oreo
                    .setShortLabel(q.title)
                    .setIcon(IconCompat.createWithBitmap(Bitmap.createScaledBitmap(q.icon,64,64, true)))
                    .build();
            ShortcutManagerCompat.requestPinShortcut(context, shortcutInfo, null);
        }
        else
        {
            Toast.makeText(getBaseContext(), "Shortcut are not supported", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setNowPlaying(SolarusApp.nowPlaying(mQuest));
    }

    public void onPlayButtonPressed() {
        Intent i = MainActivity.shortcutIntent(getApplicationContext(), mQuest);
        startActivity(i);
        finish();
    }

    public void onQuitButtonPressed() {
        SolarusApp.finishCurrentEngine();
        setNowPlaying(false);
    }

    public void onResumeButtonPressed() {
        MainActivity.questResumeRequest(getApplicationContext(), mQuest);
    }

    public void onShortcutButtonPressed() {
       questShortcutRequest(mQuest);
    }

    public void setNowPlaying(boolean value) {
        // Play button
        mPlayButton.setVisibility(value ? View.GONE : View.VISIBLE);

        // Resume button
        mResumeButton.setVisibility(value ? View.VISIBLE : View.GONE);

        // Quit button
        mQuitButton.setVisibility(value ? View.VISIBLE : View.GONE);

        // Now Playing
        mNowPlaying.setVisibility(value ? View.VISIBLE : View.GONE);
    }
}
