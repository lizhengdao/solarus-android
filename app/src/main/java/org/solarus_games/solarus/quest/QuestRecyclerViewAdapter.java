package org.solarus_games.solarus.quest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.solarus_games.solarus.R;
import org.solarus_games.solarus.SolarusApp;
import org.solarus_games.solarus.quest.QuestListFragment.OnListFragmentInteractionListener;


import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class QuestRecyclerViewAdapter extends RecyclerView.Adapter<QuestRecyclerViewAdapter.ViewHolder> {

    private final List<Quest> mValues;
    private final OnListFragmentInteractionListener mListener;

    public QuestRecyclerViewAdapter(List<Quest> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_quest_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.quest = mValues.get(position);
        holder.mTitle.setText(holder.quest.title);
        holder.mAuthor.setText(holder.quest.author);
        holder.mIcon.setImageBitmap(holder.quest.icon);
        holder.mNowPlaying.setVisibility(SolarusApp.nowPlaying(holder.quest) ? View.VISIBLE : View.GONE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onQuestDetailClick(holder.quest);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitle;
        public final TextView mAuthor;
        public final ImageView mIcon;
        public final TextView mNowPlaying;
        public Quest quest;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            //TODO setup view here
            mTitle = view.findViewById(R.id.quest_title);
            mAuthor  = view.findViewById(R.id.quest_author);
            mIcon = view.findViewById(R.id.quest_icon);
            mNowPlaying = view.findViewById(R.id.now_playing);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + quest.title + "'";
        }
    }
}
